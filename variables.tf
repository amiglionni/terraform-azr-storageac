variable "subscription_id" {
  type    = string
  default = "859821ad-2fa6-42ab-a12f-66ee1584659e"
}

variable "security_group_name" {
  type    = string
  default = "sg-default"
}

variable "virtual_network" {
  type    = string
  default = "vn-k8s-eastus2"
}

variable "resource_group" {
  type    = string
  default = "rg-default"
}

variable "location" {
  type    = string
  default = "East US 2"
}

variable "vm_worker01" {
  type    = string
  default = "worker01"
}

variable "vm_worker02" {
  type    = string
  default = "worker02"
}

variable "vm_k8s_master" {
  type    = string
  default = "k8s-master"
}

variable "admin_user" {
    default = "adminuser"
}

variable "admin_password" {
    default = "Password%1234"
}

variable "size_vm" {
    default = "Standard_B2ms"
}

variable "tags" { 
    type = "map" 
    default = { 
        Name: "value",
        Created: "Terraform",
        Owner: "Alberto Miglionni",
        Env: "Developer"
  } 
}
