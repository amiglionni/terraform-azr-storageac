resource "azurerm_storage_share" "sto" {
  name                 = "meushare"
  storage_account_name = azurerm_storage_account.sto.name
  quota                = 50
}
